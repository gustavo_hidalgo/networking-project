package fxa;

import rxp.*;

import java.io.*;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
* This class works to serve files from a root directory over an RxP connection
*/
public class FxAServer {

    private static File root;
    private static RxPServerSocket ss = new RxPServerSocket();
    static int localPort;
    static InetAddress netEmuAddress;
    static int netEmuPort;
    static Scanner scanner = new Scanner(System.in);
    static String userInput = "";

    public static void main(String[] args) throws UnknownHostException {
        try {
            localPort = Integer.parseInt(args[0]);
            netEmuAddress = InetAddress.getByName(args[1]);
            netEmuPort = Integer.parseInt(args[2]);
            System.out.println("Wating for connection on port " + localPort);
            RxPSendSocket sendSocket = (RxPSendSocket) ss.accept(localPort, true);
            System.out.println("Got a connection!");
            while(!userInput.equalsIgnoreCase("terminate")) {
                byte[] request = sendSocket.receiveOnePacket();
                System.out.println("Request made!");
                OutputStream out = sendSocket.getOutputStream();
                FxARequest parsed = new FxARequest(request);
                System.out.printf("Serving %f\n", parsed.getFileRequest());
                FileInputStream reader = new FileInputStream(parsed.getFileRequest());
                byte[] file_data = new byte[reader.available()];
                reader.read(file_data);
                byte[] response = parsed.makeResponse(file_data);
                out.write(response);
                System.out.println("You may change the window size with\n window [size]\nYou may also continue waiting for requests or terminate the server with \nterminate\n");
                System.out.print("> ");
                userInput = scanner.nextLine();
                if(userInput.startsWith("window")) {
                    System.out.println("Unsupported operation.");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
