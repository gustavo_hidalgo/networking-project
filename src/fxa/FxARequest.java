package fxa;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

/**
 * FxARequest headers have the following structure
 * VERB LENGTH PATH\n
 * DATA
 *
 * That is, 4 bytes for a verb
 * a space delimeter
 * a path length as a 4 byte number
 * a space delimeter
 * a path name that is as long as the path length but
 * not long enough to exceed to maximum request length of 256 a new line character and then optional data
 * The path must be relative to the directory the server is running on.
 */
public class FxARequest {

    private String fileRequest;
    private String command;

    byte[] makeResponse(byte[] file) {
        byte[] response_header = String.format("FILE %d %s\n", file.length, fileRequest).getBytes();
        byte[] big = new byte[file.length + response_header.length];
        System.arraycopy(response_header,0, big, 0, response_header.length);
        System.arraycopy(file, 0, big, response_header.length, file.length);
        return big;
    }

    /**
     * This constructor parses ONLY a header for an FxARequest.
     * If there is any more data than a header, an assertion is thrown.
     * @param request_raw
     */
    public FxARequest(byte[] request_raw) {
        assert(request_raw.length <= 256);
        command = new String(request_raw, 0, 4, StandardCharsets.US_ASCII);
        int length = ByteBuffer.wrap(request_raw, 5, 4).getInt();
        fileRequest = new String(request_raw, 10, length);
    }

    static byte[] makeRequest(String filename) {
        return String.format("FGET %d %s\n",filename.length(), filename).getBytes(StandardCharsets.US_ASCII);

    }

    public String getFileRequest() {
        return fileRequest;
    }

    public String getCommand() {
        return command;
    }

}
