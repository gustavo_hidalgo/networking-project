package fxa;

import rxp.RxPReceiveSocket;
import rxp.RxPSocket;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
* Created by Gustavo on 4/12/15.
*/
public class FxAClient {

    static int my_port;
    static InetAddress address;
    static int server_port;
    static Scanner input = new Scanner(System.in);
    static String[] userInput;
    static RxPReceiveSocket socket;


    public static void main(String[] args) throws UnknownHostException {
        my_port = Integer.parseInt(args[0]);
        address = InetAddress.getByName(args[1]);
        server_port = Integer.parseInt(args[2]);
        socket = new RxPReceiveSocket(InetAddress.getByAddress(new byte[]{(byte)192, (byte)168, (byte)1, (byte)69}), 2001, 2000);
        assert (my_port == server_port - 1) : "Server port must be 1 more than my port";
        userInput = new String[]{""};
        help();
        while(!userInput[0].equalsIgnoreCase("disconnect")) {
            try {
                if(socket.isConnected()) {
                    System.out.print(address+"> ");
                } else {
                    System.out.print("> ");
                }
                userInput = input.nextLine().split(" ");
                if(userInput.length == 0) continue;
                if (userInput[0].equalsIgnoreCase("get")) {
                    if(userInput.length > 1) {
                        String file = userInput[1];
                        byte[] data = getFile(file, address, server_port);
                        System.out.println("Download complete, writing to disk...");
                        File f = new File(file);
                        FileOutputStream fw = new FileOutputStream(f);
                        fw.write(data);
                        fw.close();
                        System.out.printf("Done writing.");
                    }
                } else if (userInput[0].equalsIgnoreCase("connect")) {
                    if(socket.isConnected()) {
                        System.out.println("Socket already connected.");
                    } else {
                        System.out.println("Attempting connection.");
                        socket.connect();
                        System.out.println("Connection made.");
                    }
                } else if (userInput[0].equalsIgnoreCase("put")) {
                    System.out.println("Unsupported operation.");
                } else if (userInput[0].equalsIgnoreCase("window")) {
                    System.out.println("Unsupported operation.");
                } else if (userInput[0].equalsIgnoreCase("help") || userInput[0].equalsIgnoreCase("h")) {
                    help();
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }

        }
    }

    private static void help() {
        System.out.println("\nYou may enter one of the following:");
        System.out.println("- connect");
        System.out.println("- get [file]");
        System.out.println("- put [file]");
        System.out.println("- window [size]");
        System.out.println("- disconnect");
    }

    /**
     * This method gets a file from a remote server and returns it to the user
     * in an array of bytes. This method attemps to connect over port 40000
     * @param name
     * @param address
     * @param port
     * @return
     */
    static byte[] getFile(String name, InetAddress address, int port) throws IOException {
        socket.sendOnePacket(FxARequest.makeRequest(name)); //send request!!!!
        System.out.println("UNIQUE PACKET SENT");
        InputStream in = socket.getInputStream();
        while(in.available() < 256); //wait until you can read 256 bytes
        Scanner s = new Scanner(in);
        System.out.println("Got data");
        String raw_header = s.nextLine();
        String[] request_members = raw_header.split(" ");
        assert(request_members[0].equals("FILE"));
        assert(request_members[2].equals(name));
        int toRead = Integer.parseInt(request_members[1]);
        byte[] buffer = new byte[toRead];
        int received_bytes = 0;
        while(received_bytes < toRead) {
            received_bytes += in.read(buffer, received_bytes, toRead - received_bytes);
        }
        return buffer;
    }
}
