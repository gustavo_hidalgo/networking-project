package rxp;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Stream that users write to to send over the network
 */
public class RxPOutputStream extends OutputStream {

    byte[] buffer; //the buffer this input stream manages
    private int mark; //points to the next available index to write to in buffer
    private int pos; //next index to read from
    private int waiting; //available bytes to read for sending... should not grow past buffer.length
    byte[] retransmission_buffer = new byte[1 << 15];
    int bytes_waiting_ack; //number of bytes waiting to be ackknowledged
    int retrans_position; //pointer to the start of the retransmission buffer
    int add_to_retrans; //pointer to the position to add to the retransmission buffer

    public RxPOutputStream(byte[] buffer) {
        this.buffer = buffer;
    }

    @Override
    synchronized public void write(int b) throws IOException {
        if(waiting == buffer.length) throw new IOException("Buffer is full");
        byte a = (byte)(0x00FF & b);
        buffer[mark] = a;
        mark = ((mark + 1) % buffer.length);
        waiting++;
    }

    int getWaiting() {
        return waiting;
    }

    /**
     * Gets n bytes from the retransmission buffer but does not release them
     * from the buffer
     * @param bytes number of bytes to request
     * @param off offset from the start of the retransmission buffer
     * @return a byte[] holding bytes for retransmission
     */
    byte[] getFromRetransmissionBuffer(int bytes, int off) {
        byte[] temp = new byte[bytes];
        int marker = add_to_retrans + off;
        for (int i = 0; i < bytes; i++) {
            marker = (marker + i) % retransmission_buffer.length;
            temp[i] = retransmission_buffer[marker];
        }
        return temp;
    }

    /**
     * Should probablre return the number of bytes that were actually released
     * TODO
     * @param bytes
     * @return
     */
    int releaseFromRetransmissonBuffer(int bytes) {
        bytes_waiting_ack -= bytes;
        retrans_position = (retrans_position + bytes) % retransmission_buffer.length;
        return bytes;
    }

    synchronized boolean fillsPacket() {
        return waiting > 0;
    }

    /**
     * The commensurate "read" method
     * @return a byte[] of packet data. ONLY THE DATA
     * @throws IOException
     */
    synchronized byte[] getPacket()  {
        byte[] temp;
        if(waiting < RxPPacket.MAX_LENGTH) {
            temp = new byte[waiting];
            for(int i = 0; i < waiting; i++) {
                temp[i] = buffer[pos];
                pos = (pos + 1) % buffer.length;
            }
            waiting = 0;
        } else {
            int buffer_size = RxPPacket.MAX_LENGTH - RxPPacket.HEADER_LENGTH;
            temp = new byte[buffer_size];
            for(int i = 0; i < buffer_size; i++) {
                temp[i] = buffer[pos];
                pos = (pos + 1) % buffer.length;
            }
            waiting -= buffer_size;
        }
        /*Keep the packet data for a future retransmission*/
        System.arraycopy(temp, 0, retransmission_buffer, add_to_retrans, temp.length);
        add_to_retrans = (add_to_retrans + temp.length) % retransmission_buffer.length;
        bytes_waiting_ack += temp.length;
        return temp;
    }
}
