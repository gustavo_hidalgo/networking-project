package rxp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * Created by sayurimila on 4/12/15.
 * Parent class of Send and Receive Sockets.
 */
public abstract class RxPSocket {
    protected int port;
    protected InetAddress address;
    protected int localPort;
    protected InetAddress localAddress;
    protected boolean packetSending;

    public RxPSocket(InetAddress address, int port, int localPort) {
        this.address = address;
        this.port = port;
        this.localPort = localPort;
        try {
            localAddress = InetAddress.getLocalHost();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendOnePacket(byte[] message) {
        DatagramSocket systemUDPSocket = getMySocket();
        packetSending = true;
        RxPPacket wrappedMessage = new RxPPacket(RxPPacket.ONCE,
                0,
                0,
                localPort,
                port,
                0,
                0,
                message);
        DatagramPacket p = new DatagramPacket(wrappedMessage.raw, wrappedMessage.raw.length);
        while (true) {
            if (!systemUDPSocket.isClosed()) {
                try {
                    systemUDPSocket.send(p);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                return;
            }
            DatagramPacket p1 = new DatagramPacket(new byte[RxPPacket.MAX_LENGTH], RxPPacket.MAX_LENGTH);
            int cyclesSincePcktSent = 0;
            while (cyclesSincePcktSent < 10) {
                cyclesSincePcktSent++;
                try {
                    Thread.sleep(100);
                    systemUDPSocket.setSoTimeout(100);
                    systemUDPSocket.receive(p1);
                    RxPPacket p2 = new RxPPacket(p1);
                    if (p2.checkChecksum() && p2.isACK() && p2.isONCE()) {
                        System.out.println("RECEIVED ACK FOR UNIQUE PACKET");
                        startThreads();
                        return;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public byte[] receiveOnePacket() {
        DatagramSocket systemUDPSocket = getMySocket();
        DatagramPacket p1 = new DatagramPacket(new byte[RxPPacket.MAX_LENGTH], RxPPacket.MAX_LENGTH);
        while(true) {
            try {
                systemUDPSocket.setSoTimeout(100);
                systemUDPSocket.receive(p1);
                RxPPacket p2 = new RxPPacket(p1);
                if (p2.checkChecksum() && p2.isONCE()) {
                    ((RxPInputStream)(getInputStream())).insert(p2.data, p2.packet_size);
                    /* I hope one of these gets there...  */
                    for (int i = 0; i < 3; i++) {
                        RxPPacket p3 = new RxPPacket((short)(RxPPacket.ONCE | RxPPacket.ACK),
                                0,
                                0,
                                localPort,
                                port,
                                0,
                                0,
                                new byte[0]);
                        systemUDPSocket.send(new DatagramPacket(p3.raw, p3.raw.length));
                    }
                    startThreads();
                    return p2.data;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



    protected abstract DatagramSocket getMySocket();
    /**
     * For the user to be able to read incoming packets
     * @return an InputStream
     */
    public abstract InputStream getInputStream();

    /**
     * For the user to send data out.
     * @return An OutputStream
     */
    public abstract OutputStream getOutputStream();

    protected abstract void startThreads();
}
