package rxp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.*;

/**
 * Created by sayurimila on 4/12/15.
 */
public class RxPSendSocket extends RxPSocket {

    private DatagramSocket systemUDPSocket;
    private RxPOutputStream outStream; //to network
    private RxPInputStream inStream; //to user
    private Thread sendThread;
    private boolean connected;
    private boolean expectingAck;
    private int bytesLastSent;

    private enum STATE {WAIT, SEND_DATA, WAIT_FOR_ACK, RETRANSMIT_DATA}
    private STATE mystate = STATE.WAIT;

    private int seqn = 0;
    private int ackn = 0;

    private int cyclesSinceDataSent = 0;

    public RxPSendSocket(InetAddress address, int port, int localPort) {
        super(address, port, localPort);
        try {
            systemUDPSocket = new DatagramSocket(localPort, localAddress);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Only rxp.RxPServerSocket should call this.
     * @param address
     * @param port
     * @param localPort
     * @param systemUDPSocket
     */
    RxPSendSocket(InetAddress address, int port, int localPort, DatagramSocket systemUDPSocket) {
        super(address, port, localPort);
        try {
            connected = true;
            inStream = new RxPInputStream(new byte[1 << 21]);
            outStream = new RxPOutputStream(new byte[1 << 21]);
            this.systemUDPSocket = systemUDPSocket;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Connects the socket to another socket specified by IPAddress address and int port.
     */
    public void connect() {
        if(connected) return;
        systemUDPSocket.connect(address, port);
        /* Handshake */
        RxPPacket connection_maker = new RxPPacket((RxPPacket.SYN),
                0, //pick a random sequence number
                0,
                localPort,
                port,
                1024,
                1,
                new byte[0]);
        DatagramPacket packet = new DatagramPacket(connection_maker.raw, connection_maker.raw.length);
        try {
            systemUDPSocket.send(packet);
            //timeout here
            systemUDPSocket.receive(packet);
            connection_maker = new RxPPacket(packet);
            if (!connection_maker.checkChecksum() || !(connection_maker.isACK() || connection_maker.isSYN())) {
                throw new SocketException("Bogus connection");
            }
            //send back an ACK Packet
            connection_maker = new RxPPacket(RxPPacket.ACK,
                    connection_maker.ack_n,
                    connection_maker.seq_n + 1,
                    localPort,
                    port,
                    1024,
                    1,
                    new byte[0]);
            packet = new DatagramPacket(connection_maker.raw, connection_maker.raw.length);
            systemUDPSocket.send(packet); //good to go!
            inStream = new RxPInputStream(new byte[1 << 21]);
            outStream = new RxPOutputStream(new byte[1 << 21]);
            connected = true;
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    @Override
    protected void startThreads() {
        sendThread = sendThread();
        sendThread.start();
    }

    /**
     * Closes the socket and releases system resources associated with the socket streams.
     */
    public void close() {
        if(!connected) return;
//        outStream.close(); maybe
        while(outStream.fillsPacket() || packetSending) {
            //System.out.println("Things left to do");
        }
        connected = false;
        RxPPacket connection_ender = new RxPPacket((RxPPacket.FIN),
                0,
                0,
                localPort,
                port,
                1024,
                0,
                "FIN PACKET".getBytes());
        DatagramPacket packet = new DatagramPacket(connection_ender.raw, connection_ender.raw.length);
        try{
            Thread.sleep(100);
            System.out.println("Connection closing");
            systemUDPSocket.send(packet);
            System.out.println("Sent FIN");
            connection_ender = new RxPPacket((RxPPacket.FIN), 0,0,localPort, port, 1024, 0, "FIN ACK".getBytes());
            packet = new DatagramPacket(connection_ender.raw, connection_ender.raw.length);
            systemUDPSocket.setSoTimeout(2000);
            //receiveThread.interrupt();
            System.out.println("Waiting on FINACK");
            systemUDPSocket.receive(packet);
            System.out.println("Received FINACK");
            connection_ender = new RxPPacket(packet);
            if(!connection_ender.checkChecksum() || !(connection_ender.isFIN() && connection_ender.isACK())) {
                throw new SocketException("Dirty close");
            }
            connection_ender =  new RxPPacket((RxPPacket.ACK),
                    0,
                    0,
                    localPort,
                    port,
                    1024,
                    0,
                    "ACK PACKET".getBytes());
            packet = new DatagramPacket(connection_ender.raw, connection_ender.raw.length);
            systemUDPSocket.send(packet);
            System.out.println("Sent final ACK");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        releaseResources();
        System.out.println("Resources released");
    }

    private void releaseResources() {
        connected = false;
        sendThread.interrupt();
        if(systemUDPSocket.isConnected()) {
            systemUDPSocket.disconnect();
        }
        systemUDPSocket.close();
    }

    synchronized private void sendData(int seq_n, int ack_n) throws IOException {
        packetSending = true;
        byte[] message = outStream.getPacket();
        RxPPacket wrappedMessage = new RxPPacket((short)0,
                seq_n,
                ack_n,
                localPort,
                port,
                0,
                0,
                message);
        bytesLastSent = message.length;
        DatagramPacket p = new DatagramPacket(wrappedMessage.raw, wrappedMessage.raw.length);
        if(!systemUDPSocket.isClosed()) {
            systemUDPSocket.send(p);
            System.out.println("SENT " + wrappedMessage.packet_size + " bytes, S: " + wrappedMessage.seq_n + ", A: " + wrappedMessage.ack_n);

            expectingAck = true;
        }
        packetSending = false;
    }

    synchronized private void retransData(int seq_n, int ack_n) throws IOException {
        packetSending = true;
        byte[] toRetrans = outStream.getFromRetransmissionBuffer(bytesLastSent, 0);
        RxPPacket wrappedMessage = new RxPPacket((short)0,
                seq_n,
                ack_n,
                localPort,
                port,
                0,
                0,
                toRetrans);
        DatagramPacket p = new DatagramPacket(wrappedMessage.raw, wrappedMessage.raw.length);
        if(!systemUDPSocket.isClosed()) {
            systemUDPSocket.send(p);
            System.out.println("RETRANSMITTED " + wrappedMessage.packet_size + " bytes, S: " + wrappedMessage.seq_n + ", A: " + wrappedMessage.ack_n);

            expectingAck = true;
        }
        packetSending = false;
    }

    Thread sendThread() {
        return new Thread(() -> {
            while(connected) {
                cyclesSinceDataSent++;
                try {
                    systemUDPSocket.setSoTimeout(100);
                    switch (mystate) {
                        case WAIT:
                            while(!outStream.fillsPacket());
                            mystate = STATE.SEND_DATA;
                            expectingAck = false;
                            System.out.println("SEND STATE CHANGED TO SEND_DATA");
                            break;
                        case SEND_DATA:
                            try {
                                sendData(seqn, ackn);
                                cyclesSinceDataSent = 0;
                                seqn++;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            expectingAck = true;
                            mystate = STATE.WAIT_FOR_ACK;
                            break;
                        case WAIT_FOR_ACK:
                            if (cyclesSinceDataSent > 10) {
                                mystate = STATE.RETRANSMIT_DATA;
                                break;
                            }
                            DatagramPacket p1 = new DatagramPacket(new byte[RxPPacket.MAX_LENGTH], RxPPacket.MAX_LENGTH);
                            System.out.println("WAITING FOR ACK PACKET");
                            systemUDPSocket.receive(p1); //I assume this blocks for up to 100ms
                            RxPPacket p = new RxPPacket(p1);
                            if(p.checkChecksum()) {
                                System.out.println("RECEIVED PACKET: Length " + p.packet_size + ", S " + p.seq_n + ", A " + p.ack_n);
                                if (p.isFIN()) {
                                    System.out.println("RECEIVED FIN");
                                    if(connected) startClosing();
                                }
                                if ((p.ack_n == seqn) && (p.isACK())) {
                                    System.out.println("LAST PACKET SENT WAS ACKED");
                                    ackn = p.seq_n + 1;
                                    expectingAck = false;
                                    mystate = STATE.WAIT;
                                }
                                if (p.seq_n == ackn) {
                                    System.out.println("PACKETS ARE IN ORDER");
                                }
                            } else if (!p.checkChecksum()) {
                                System.out.println("CHECKSUM FAILED");
                            }
                            break;
                        case RETRANSMIT_DATA:
                            retransData(seqn - 1, ackn);
                            cyclesSinceDataSent = 0;
                            mystate = STATE.WAIT_FOR_ACK;
                            break;
                    }
                } catch (SocketTimeoutException e) {
                    if (mystate == STATE.WAIT_FOR_ACK) {
                        System.out.println("TIMEDOUT");
                        mystate = STATE.RETRANSMIT_DATA;
                    }
//                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void startClosing() {
        connected = false;
        try {
            RxPPacket connection_maker = new RxPPacket((short)((RxPPacket.FIN) | (RxPPacket.ACK)), 0,0,localPort, port, 1024, 0, "FIN ACK".getBytes());
            DatagramPacket packet = new DatagramPacket(connection_maker.raw, connection_maker.raw.length);
            systemUDPSocket.send(packet);
            System.out.println("Sent FINACK");
            systemUDPSocket.setSoTimeout(1000);
            systemUDPSocket.receive(packet); //receive final ack
            System.out.println("Received final ACK");
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        releaseResources();
        System.out.println("Resources released");
    }

    public boolean isConnected() {
        return connected;
    }

    /**
     * For the user to be able to read incoming packets
     * @return an InputStream
     */
    @Override
    public InputStream getInputStream() {
        return inStream;
    }

    /**
     * For the user to send data out.
     * @return An OutputStream
     */
    @Override
    public OutputStream getOutputStream() {
        return outStream;
    }

    @Override
    protected DatagramSocket getMySocket() {
        return systemUDPSocket;
    }
}