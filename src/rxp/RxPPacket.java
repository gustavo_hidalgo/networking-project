package rxp;

import java.net.DatagramPacket;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * Created by Gustavo on 4/8/15.
 */
public class RxPPacket {

    public final static int MAX_LENGTH = 1024;
    public final static int HEADER_LENGTH = 32;
    byte[] raw;
    byte[] data;
    public final int header_size;
    public final int packet_size;
    public final int seq_n;
    public final int checksum;
    public final int ack_n;
    public final int src_port, dst_port;
    public final int flow_window;
    public final int flags;
    public final int window_size;

    public static final short SYN = 0x0001;
    public static final short ACK = 0x0002;
    public static final short FIN = 0x0004;
    public static final short ONCE = 0x0008;


    /**
     * Constructs an rxp.RxPPacket with the specified parameters
     * @param flags control flags
     * @param seq_n a sequence number
     * @param ack_n an ack_number
     * @param src_port a src_port
     * @param dst_port a destination port
     * @param flow_window a flow window size
     * @param window_size a sliding window size
     * @param data the user data to put in this packet
     */
    public RxPPacket(short flags, int seq_n, int ack_n, int src_port, int dst_port, int flow_window, int window_size, byte[] data) {
        this.raw = new byte[data.length + HEADER_LENGTH];
        this.data = new byte[data.length];
        System.arraycopy(data,0 ,this.data, 0, data.length); //copy into data
        System.arraycopy(data, 0, raw, HEADER_LENGTH, data.length); //copy into raw
        header_size = HEADER_LENGTH;
        raw[0] = (byte)((HEADER_LENGTH & 0xFF00) >> 8);
        raw[1] = (byte)(HEADER_LENGTH & 0x00FF);
        packet_size = data.length;
        raw[2] = (byte)((packet_size & 0xFF00) >> 8);
        raw[3] = (byte)(packet_size & 0x00FF);
        this.seq_n = seq_n;
        raw[4] = (byte)((seq_n & 0xFF000000) >> 24);
        raw[5] = (byte)((seq_n & 0x00FF0000) >> 16);
        raw[6] = (byte)((seq_n & 0x0000FF00) >> 8);
        raw[7] = (byte)((seq_n & 0x000000FF));
        this.ack_n = ack_n;
        raw[12] = (byte)((ack_n & 0xFF000000) >> 24);
        raw[13] = (byte)((ack_n & 0x00FF0000) >> 16);
        raw[14] = (byte)((ack_n & 0x0000FF00) >> 8);
        raw[15] = (byte)((ack_n & 0x000000FF));
        this.src_port = src_port;
        raw[16] = (byte)((src_port & 0xFF00) >> 8);
        raw[17] = (byte)((src_port & 0x00FF));
        this.dst_port = dst_port;
        raw[18] = (byte)((dst_port & 0xFF00) >> 8);
        raw[19] = (byte)((dst_port & 0x00FF));
        this.flow_window = flow_window;
        raw[20] = (byte)((flow_window & 0xFF000000) >> 24);
        raw[21] = (byte)((flow_window & 0x00FF0000) >> 16);
        raw[22] = (byte)((flow_window & 0x0000FF00) >> 8);
        raw[23] = (byte)((flow_window & 0x000000FF));
        this.flags = flags;
        raw[24] = (byte)((flags & 0xFF00) >> 8);
        raw[25] = (byte)((flags & 0x00FF));
        this.window_size = window_size;
        raw[26] = (byte)((window_size & 0xFF00) >> 8);
        raw[27] = (byte)((window_size & 0x00FF));
        this.checksum = checksumOverPacket(raw);
        raw[8] = (byte)((checksum &  0xFF000000) >> 24);
        raw[9] = (byte)((checksum &  0x00FF0000) >> 16);
        raw[10] = (byte)((checksum & 0x0000FF00) >> 8);
        raw[11] = (byte)((checksum & 0x000000FF));
    }

    public boolean isSYN() {
        return (flags & 0x0001) != 0;
    }

    public boolean isFIN() {
        return (flags & 0x0004) != 0;
    }

    public boolean isACK() {
        return (flags & 0x0002) != 0;
    }

    public boolean isONCE() {
        return (flags & 0x0008) != 0;
    }

    /**
     * Constructor used to turn a UDP packet into an rxp.RxPPacket
     * @param UDPpacket the UDP packet received
     */
    RxPPacket(DatagramPacket UDPpacket) {
        byte[] whole_packet = UDPpacket.getData();
        raw = whole_packet;
        java.nio.ByteBuffer bb = java.nio.ByteBuffer.wrap(whole_packet);
        header_size = bb.getShort(0);
        packet_size = bb.getShort(2);
        seq_n = bb.getInt(4);
        checksum = bb.getInt(8);
        ack_n = bb.getInt(12);
        src_port = bb.getShort(16);
        dst_port = bb.getShort(18);
        flow_window = bb.getInt(20);
        flags = bb.getShort(24);
        window_size = bb.getShort(26);
        data = new byte[packet_size];
        for (int i = 0; i < packet_size - header_size; i++) {
            data[i] = whole_packet[header_size + i];
        }
    }

    public boolean checkChecksum() {
        //System.out.printf("Checking checksum: %d header size and %d packet size\n", header_size, packet_size);
        byte[] temp = new byte[header_size - 4 + packet_size];
        //read header without checksum
        System.arraycopy(raw, 0, temp, 0, 8);
        //rest of stuff
        System.arraycopy(raw, 12, temp, 8, 20 + packet_size);
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte[] digested = md.digest(temp);
        int dnumber =  ByteBuffer.wrap(new byte[]{digested[12], digested[13], digested[14], digested[15]}).getInt();
        return dnumber == checksum;
    }

    private int checksumOverPacket(byte[] stuff) {
        byte[] temp = new byte[HEADER_LENGTH - 4 + packet_size]; //read the packet length
        //read header without checksum
        System.arraycopy(raw, 0, temp, 0, 8);
        //rest of stuff
        System.arraycopy(raw, 12, temp, 8, 20 + packet_size);
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte[] digested = md.digest(temp);
        return ByteBuffer.wrap(new byte[]{digested[12], digested[13], digested[14], digested[15]}).getInt();
    }

    @Override
    public String toString() {
        return "RxPPacket{" +
                "header_size=" + header_size + "B"+
                ", packet_size=" + packet_size + "B"+
                ", seq_n=" + seq_n +
                ", checksum=" + checksum +
                ", ack_n=" + ack_n +
                ", src_port=" + src_port +
                ", dst_port=" + dst_port +
                ", flow_window=" + flow_window +
                ", flags=" + flags +
                ", window_size=" + window_size +
                '}';
    }
}
