package rxp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * Created by Gustavo on 3/29/15.
 */
public class RxPServerSocket {

    int localPort;
    static private Set<Integer> usedPorts = new HashSet<>();
    private InetAddress localAddress;


    public RxPSocket accept(int localPort, boolean sendSocket) throws SocketException {
        System.out.println("LISTENING ON PORT: " + localPort);
        if(usedPorts.contains(localPort)) {
            throw new SocketException("Port is already bound");
        } else {
            usedPorts.add(localPort);
        }
        this.localPort = localPort;
        try {
            localAddress = InetAddress.getLocalHost();
        } catch (Exception e) {
            e.printStackTrace();
        }
        RxPSocket created = null;
        try {
            /* Handshake!  */
            DatagramSocket socket = new DatagramSocket(localPort);
            DatagramPacket packet = new DatagramPacket(new byte[RxPPacket.MAX_LENGTH], RxPPacket.MAX_LENGTH);
            socket.receive(packet);
            RxPPacket parsed = new RxPPacket(packet);
            System.out.println("RECEIVED PACKET");
            if(!(parsed.checkChecksum() && parsed.isSYN())) {
                throw new SocketException("Bogus connection");
            }
            //now in SYN_RCV state
            System.out.println("PACKET RECEIVED IS SYN");
            RxPPacket response = new RxPPacket((short)(RxPPacket.ACK | RxPPacket.SYN),
                    0, //pick a random sequence number
                    0, //respond with their sequence number + 1
                    localPort,
                    packet.getPort(),
                    1024,
                    1,
                    new byte[0]);
            packet = new DatagramPacket(response.raw, response.raw.length, packet.getAddress(), packet.getPort());
            socket.send(packet);
            System.out.println("SENT SYNACK");
            packet = new DatagramPacket(new byte[RxPPacket.MAX_LENGTH], RxPPacket.MAX_LENGTH);
            socket.receive(packet);
            parsed = new RxPPacket(packet);
            System.out.println("RECEIVED ACK");
            if(!(parsed.checkChecksum() && parsed.isACK())) {
                throw new SocketException("Bogus connection");
            }
            //socket.connect(packet.getAddress(), packet.getPort());
            socket.close();
            socket = new DatagramSocket(localPort, localAddress);
            socket.connect(packet.getAddress(), packet.getPort());
            /* special constructor */
            if (sendSocket) {
                created = new RxPReceiveSocket(localAddress, packet.getPort(), localPort, socket);
            } else {
                created = new RxPSendSocket(localAddress, packet.getPort(), localPort, socket);
            }

        } catch (IOException s) {
            s.printStackTrace();
        }
        return created;
    }


    public void close() {
        // Release resources, close spawned sockets that serve clients, stop accepting
    }

}
