package rxp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.*;

/**
 * Created by sayurimila on 4/12/15.
 */
public class RxPReceiveSocket extends RxPSocket {

    private DatagramSocket systemUDPSocket;
    private RxPOutputStream outStream; //to network
    private RxPInputStream inStream; //to user
    private Thread receiveThread;
    private boolean connected;

    private enum STATE {SEND_ACK, WAIT_FOR_DATA, RETRANSMIT_ACK}
    private STATE mystate = STATE.WAIT_FOR_DATA;

    private int seqn = 0;
    private int ackn = 0;

    private int cyclesSinceAckSent = 0;

    public RxPReceiveSocket(InetAddress address, int port, int localPort) {
        super(address, port, localPort);
        try {
            systemUDPSocket = new DatagramSocket(localPort, localAddress);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void startThreads() {
        receiveThread = receiveThread();
        receiveThread.start();
    }

    /**
     * Only rxp.RxPServerSocket should call this.
     * @param address
     * @param port
     * @param localPort
     * @param systemUDPSocket
     */
    RxPReceiveSocket(InetAddress address, int port, int localPort, DatagramSocket systemUDPSocket) {
        super(address, port, localPort);
        try {
            connected = true;
            inStream = new RxPInputStream(new byte[1 << 21]);
            outStream = new RxPOutputStream(new byte[1 << 21]);
            this.systemUDPSocket = systemUDPSocket;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Connects the socket to another socket specified by IPAddress address and int port.
     */
    public void connect() {
        if(connected) return;
        systemUDPSocket.connect(address, port);
        System.out.println("Connected SystemUDP");
        /* Handshake */
        RxPPacket connection_maker = new RxPPacket((RxPPacket.SYN),
                0, //pick a random sequence number
                0,
                localPort,
                port,
                1024,
                1,
                new byte[0]);
        DatagramPacket packet = new DatagramPacket(connection_maker.raw, connection_maker.raw.length);
        try {
            systemUDPSocket.send(packet);
            System.out.println("SYN SENT");
            //timeout here
            systemUDPSocket.receive(packet);
            System.out.println("SYNACK RECEIVED");
            connection_maker = new RxPPacket(packet);
            if (!connection_maker.checkChecksum() || !(connection_maker.isACK() || connection_maker.isSYN())) {
                throw new SocketException("Bogus connection");
            }
            //send back an ACK Packet
            connection_maker = new RxPPacket(RxPPacket.ACK,
                    connection_maker.ack_n,
                    connection_maker.seq_n + 1,
                    localPort,
                    port,
                    1024,
                    1,
                    new byte[0]);
            packet = new DatagramPacket(connection_maker.raw, connection_maker.raw.length);
            systemUDPSocket.send(packet); //good to go!
            System.out.println("SENT ACK");
            inStream = new RxPInputStream(new byte[1 << 21]);
            outStream = new RxPOutputStream(new byte[1 << 21]);
            connected = true;
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * Closes the socket and releases system resources associated with the socket streams.
     */
    public void close() {
        if(!connected) return;
//        outStream.close(); maybe
        while(outStream.fillsPacket() || packetSending) {
            //System.out.println("Things left to do");
        }
        connected = false;
        RxPPacket connection_ender = new RxPPacket((RxPPacket.FIN),
                0,
                0,
                localPort,
                port,
                1024,
                0,
                "FIN PACKET".getBytes());
        DatagramPacket packet = new DatagramPacket(connection_ender.raw, connection_ender.raw.length);
        try{
            Thread.sleep(100);
            System.out.println("Connection closing");
            systemUDPSocket.send(packet);
            System.out.println("Sent FIN");
            connection_ender = new RxPPacket((RxPPacket.FIN), 0,0,localPort, port, 1024, 0, "FIN ACK".getBytes());
            packet = new DatagramPacket(connection_ender.raw, connection_ender.raw.length);
            systemUDPSocket.setSoTimeout(2000);
            //receiveThread.interrupt();
            System.out.println("Waiting on FINACK");
            systemUDPSocket.receive(packet);
            System.out.println("Received FINACK");
            connection_ender = new RxPPacket(packet);
            if(!connection_ender.checkChecksum() || !(connection_ender.isFIN() && connection_ender.isACK())) {
                throw new SocketException("Dirty close");
            }
            connection_ender =  new RxPPacket((RxPPacket.ACK),
                    0,
                    0,
                    localPort,
                    port,
                    1024,
                    0,
                    "ACK PACKET".getBytes());
            packet = new DatagramPacket(connection_ender.raw, connection_ender.raw.length);
            systemUDPSocket.send(packet);
            System.out.println("Sent final ACK");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        releaseResources();
        System.out.println("Resources released");
    }

    private void releaseResources() {
        connected = false;
        receiveThread.interrupt();
        if(systemUDPSocket.isConnected()) {
            systemUDPSocket.disconnect();
        }
        systemUDPSocket.close();
    }

    synchronized private void sendACK(int seq_n, int ack_n) throws IOException {
        packetSending = true;
        RxPPacket ackPacket = new RxPPacket(RxPPacket.ACK,
                seq_n,
                ack_n,
                localPort,
                port,
                1024,
                0,
                new byte[0]);
        DatagramPacket p1 = new DatagramPacket(ackPacket.raw, ackPacket.raw.length);
        systemUDPSocket.send(p1);
        packetSending = false;
    }


    Thread receiveThread(){
        return new Thread(() -> {
            while(connected) {
                cyclesSinceAckSent++;
                try {
                    DatagramPacket p1 = new DatagramPacket(new byte[RxPPacket.MAX_LENGTH], RxPPacket.MAX_LENGTH);
                    //systemUDPSocket.setSoTimeout(200);
                    switch (mystate) {
                        case SEND_ACK:
                            sendACK(seqn, ackn);
                            cyclesSinceAckSent = 0;
                            System.out.println("ACK SENT: S " + seqn + ", A " + ackn);
                            seqn++;
                            mystate = STATE.WAIT_FOR_DATA;
                            break;
                        case WAIT_FOR_DATA:
                            if (cyclesSinceAckSent > 10) {
                                mystate = STATE.RETRANSMIT_ACK;
                                break;
                            }
                            System.out.println("WAITING FOR DATA");
                            systemUDPSocket.receive(p1); //I assume this blocks for up to 100ms
                            RxPPacket p = new RxPPacket(p1);
                            if(p.checkChecksum()) {
                                System.out.println("RECEIVED PACKET: Length " + p.packet_size + ", S " + p.seq_n + ", A " + p.ack_n);
                                if (p.isFIN()) {
                                    System.out.println("RECEIVED FIN");
                                    if(connected) startClosing();
                                }
                                if ((p.seq_n == ackn - 1) && (p.packet_size > 0)) {
                                    mystate = STATE.RETRANSMIT_ACK;
                                    break;
                                }
                                if ((p.seq_n == ackn) && (p.packet_size > 0)) {
                                    System.out.println("RECEIVED SEQUENTIAL DATA PACKET");
                                    System.out.println("EXPECTED S: " + ackn + " RECEIVED S: " + p.seq_n);
                                    inStream.insert(p.data, p.packet_size);
                                    ackn = p.seq_n + 1;
                                    mystate = STATE.SEND_ACK;
                                }
                                if (p.ack_n == seqn) {
                                    System.out.println("RECEIVED PACKET ACKED OUR PREVIOUSLY SENT PACKET");
                                }
                            } else if (!p.checkChecksum()) {
                                System.out.println("CHECKSUM FAILED");
                            }
                            break;
                        case RETRANSMIT_ACK:
                            sendACK(seqn - 1, ackn);
                            System.out.println("ACK RESENT: S " + (seqn-1) + ", A " + ackn);
                            cyclesSinceAckSent = 0;
                            mystate = STATE.WAIT_FOR_DATA;
                            break;
                    }
                } catch (SocketTimeoutException e) {
                    if (mystate == STATE.WAIT_FOR_DATA) {
                        System.out.println("TIMEDOUT");
                        mystate = STATE.RETRANSMIT_ACK;
                    }
//                      e.printStackTrace();
                } catch (Exception e) {
                      e.printStackTrace();
                }
            }
        });
    }

    private void startClosing() {
        connected = false;
        try {
            RxPPacket connection_maker = new RxPPacket((short)((RxPPacket.FIN) | (RxPPacket.ACK)), 0,0,localPort, port, 1024, 0, "FIN ACK".getBytes());
            DatagramPacket packet = new DatagramPacket(connection_maker.raw, connection_maker.raw.length);
            systemUDPSocket.send(packet);
            System.out.println("Sent FINACK");
            systemUDPSocket.setSoTimeout(1000);
            systemUDPSocket.receive(packet); //receive final ack
            System.out.println("Received final ACK");
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        releaseResources();
        System.out.println("Resources released");
    }

    public boolean isConnected() {
        return connected;
    }

    /**
     * For the user to be able to read incoming packets
     * @return an InputStream
     */
    public InputStream getInputStream() {
        return inStream;
    }

    /**
     * For the user to send data out.
     * @return An OutputStream
     */
    public OutputStream getOutputStream() {
        return outStream;
    }


    @Override
    protected DatagramSocket getMySocket() {
        return systemUDPSocket;
    }
}