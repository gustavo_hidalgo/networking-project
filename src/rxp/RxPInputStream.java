package rxp;

import java.io.IOException;
import java.io.InputStream;

/**
 * This stream is the User facing stream where RxP clients
 * can read data that was received over the network.
 */
public class RxPInputStream extends InputStream {

    //the buffer this input stream manages
    byte[] buffer;
    //mark points to the next availableToWrite index to write to in the buffer
    private int mark;
    //pos points to the next availableToWrite index to read from in the buffer
    private int pos;
    //availableToWrite bytes to write to.
    private int availableToWrite;


    /**
     * method to be used only by other RxP classes to write new data into the buffer
     */
    int insert(byte[] newData, int length) {
        if(length > availableToWrite) return 0;
        for (int i = 0; i < length; i++) {
            buffer[mark] = newData[i];
            mark = ((mark + 1) % buffer.length); //wrap around
        }
        availableToWrite -= length;
        return length;
    }

    /**
     * Construct an rxp.RxPInputStream with a specified size
     * @param buffer the buffer this inputstream manages
     */
    public RxPInputStream(byte[] buffer) {
        this.buffer = buffer;
        availableToWrite = buffer.length;
    }

    //available to read!
    @Override
    public int available() throws IOException {
        return buffer.length - availableToWrite;
    }

    @Override
    /**
     * aaah inputstream you so smart so smart so smart
     */
    public int read(byte[] b, int off, int len) throws IOException {
        int read = 0;
        while(available() != 0 && read < len) {
            b[off + read++] = (byte)read();
        }
        return read;
    }

    @Override
    /**
     * Returns the next byte from the stream
     * Updates state variables as well
     */
    public int read() throws IOException {
        if(availableToWrite == buffer.length) throw new IOException("Empty stream");
        byte a = buffer[pos];
        pos = (pos + 1 % buffer.length);
        availableToWrite++;
        return a;
    }
}
