
#silly latex needing to be compiled twice
report: 
	pdflatex *.tex
	pdflatex *.tex
	open report.pdf

clean:
	rm -rf *.aux *.log *.toc

pull:
	git pull

push:   pull
	git add -A
	git commit
	git push

