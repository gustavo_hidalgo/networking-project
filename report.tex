\documentclass[titlepage]{article}
\usepackage{graphicx}
\usepackage{fullpage}
\usepackage{subcaption}
\usepackage{listings}
\usepackage{courier}
\usepackage{hyperref}
\usepackage{tikz}

\lstset{basicstyle=\footnotesize\ttfamily,breaklines=true}

\title{Reliable Transfer Protocol}
\author{Gustavo Hidalgo \and Camila Sato}
\begin{document}
\maketitle

%extra shit
%Java NIO : http://docs.oracle.com/javase/7/docs/api/java/nio/package-summary.html#package_description
%           C level buffer control

\pagebreak

\tableofcontents
\pagebreak

\section{Overview}
    This document describes the specification for a new transport protocol.
    The Reliable Transfer Protocol (RxP) is designed to be reliable and connection-oriented, providing flow control and byte-stream communication. The protocol offers the following features:
    \begin{enumerate}
        \item Pipelined packet transmission.
        \item Full duplex communication.
        \item Corruption detection and recovery.
        \item Lost packet detection and recovery.
        \item Duplicate packet detection and recovery.
    \end{enumerate}
    \subsection{Pipelining and ACK Handling}
        RxP supports sending more than one packet simultaneously in a variable size "window".
        After connection establishment, the sender will begin by sending $4$ data packets (or less if the message is not longer than 4 packets) and wait for a cumulative ACK from the sender indicating that all packets were received.
        The sender will then increase the window size. The increase and decrease
        of the transmission window is a function of the round-trip time (RTT) and
        the frequency of lost/dropped/corrupted/out-of-order packets.
        
        The receiver will generate a cumulative ACK packet by populating the
        ack number header field with sequence number it expects to receive next from the sender.
        This means that the receiver calculates the ack number by counting the number of bytes
        consecutively received since the last ACK was sent and adding that number to the last sent ack number.
        This allows the sequence number and ACK number to start at an arbitrary value and 
        increase together. 
    \subsection{Packet Loss}
        The receiver does not send NACKs but in the case that a packet is
        dropped or reordered, the sender will receive an ACK with an ack number that is less
        than the ack number it expects to receive. The sender will resend the data
        starting from the ack number it did receive.

        On the receiver's side, ack numbers are generated after a timeout or
        a discontinuity of received sequence numbers is detected.
    \subsection{Out-of-order Delivery}
        The receiver will drop packets received out of order and send and ACK
        only for the packets that were delivered with continuous sequence numbers.
    \subsection{Duplex Communication}
        A host on either end of the connection can send and receive simultaneously and demultiplex
        based on port numbers. Both ends of the connection maintain send and receive
        buffers for this purpose.
    \subsection{Error Detection}
        A checksum over both the header and the data (shown by the packet size) is calculated by the sender and included in the header.
        The checksum does not include the checksum field obviously.
        The checksum algorithm used will be the MD5 digest and the least significant 32 bits will be placed in the checksum field.
        A receiver will recompute the checksum on a received packet and accept the packet if the checksum matches or drop the packet if the checksum is invalid. 
    \subsection{Retransmission and Error Recovery}
        Retransmission happens when packets are dropped and fail to be acknowledged due to an error detection, packet loss, out-of-order delivery, timeout or duplicate detection.
    \subsection{Duplicate Packet Detection}
        The receiver will only acknowledge sequential packets arriving consecutively. If a packet
        breaks the sequential arrival order, it is dropped. A duplicate packet would 
        break this order with a sequence number that is less than the expected next by the receiver.
        \subsection{Flow Control}
Flow control is managed by the flow-window field of the packet header.
A sender should use the flow-window that a receiver sets in order to control
the rate of data transmission. The sender should not send more bytes than what the
flow window specifies.

On the client's side, an ACK with a duplicate ACK sequence number must be generated
after a packet with a window size of $0$ is sent.
This is to inform the sender that there is more space in the receive buffer
and that they may resume sending data. We leave it to implementers to decide 
when this "Resume Transmission" ACK should be generated. 
%\pagebreak
\section{Header}
The header structure of an RxP packet follows:
\begin{center}
\begin{lstlisting}[showspaces=false]
                                      31
0   ---------------------------------------------------------------------
    | header size   | packet size     | sequence number                 |
64  ---------------------------------------------------------------------              
    | checksum                        | ack number                      |
128 ---------------------------------------------------------------------
    | src port      | dst port        | flow window                     |
192 ---------------------------------------------------------------------
    | flags         | window size     |                                 |
    ---------------------------------------------------------------------
    |                              DATA                                 |
    ---------------------------------------------------------------------
\end{lstlisting}
\end{center}

All field lengths are powers of 2.

\subsection{Header size} 16-bit field that denotes the number of bits in the header.
\subsection{Packet size} 16-bit field that denotes the number of bytes in the data of the packet.
\subsection{Sequence number} 32-bit field that denotes the sequence number of the packet.
\subsection{Checksum} 32-bit field that denotes the checksum of both the header and the packet data.
\subsection{ACK number} 32-bit field that denotes the ACK sequence number.
\subsection{Port numbers} 2 16-bit port selectors.
\subsection{Flow-window} 32-bit field that tells the sender how many bytes are left in the receiver's receive buffer.
\subsection{Flags} 16-bit field that denotes 32 bits of control flags for this RxP packet.
\subsection{Window-size} 16-bit field that tells the receiver how many packets are currently being pipelined.

%\pagebreak
\section{Connection State Diagram}
Similar to TCP, RxP is a connection oriented protocol where both client and server
maintain resources specific to a single connection. The state machine of the connection
establishment, steady state operation, and termination follows.
\begin{center}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{State.png}
    \caption{RxP State Diagram}
\end{figure}
\end{center}
The values of the sequence numbers exchanged through the connection establishment
follow the following semantics: 
\begin{enumerate}
    \item Client's SYN packet picks an arbitrary sequence number.
    \item Server receives the SYN packet and sends a SYN+ACK packet with an ACK
    sequence number that is one more than the SYN's sequence number.
    \item Client receives the SYN+ACK packet and responds with an ACK packet 
    whose ACK sequence number is one more than the received SYN+ACK packet.
\end{enumerate}

At this point, the connection is established and data transfer begins.

The closing sequence has similar semantics and the FIN packets are treated as ACK
for sequence number generation.

\section{Features}
\subsection{Flags}
Flags in the header can be individually turned on but the receiver will only
honor the flags depending on what state in the connection it is in. For instance,
a FIN flag will be ignored while the connection establishment handshake is happening.

\begin{enumerate}
    \item End-of-transaction flag: the last packet that a sender sends for a transaction
          has a bit flag turned on indicating that the transaction is finished.
    \item SYN flag: Used for connection establishment (SYNchronize).
    \item ACK flag: Used to acknowledge data reception.
    \item FIN flag: Finalize the connection. Used during tear-down.
\end{enumerate}

\section{API}
Implementations of this protocol should follow "Socket" API calls.
\subsection{rxp.RxPServerSocket}
\begin{enumerate}
	\item rxp.RxPServerSocket(int localPort): creates a rxp.RxPServerSocket and binds it to the localPort supplied.
	\item accept(): blocking call that listens for a connection to be made to the rxp.RxPServerSocket, and accepts it.
	\item close(): closes the socket.
\end{enumerate}
\subsection{rxp.RxPSocket}
\begin{enumerate}
	\item rxp.RxPSocket(IPAddress address, int port, int localPort): creates a rxp.RxPSocket and connects it to another socket specified by IPAddress address and int port. The newly created socket will bind to the localPort supplied.
	\item close(): closes the socket and releases system resources associated with the socket streams.
	\item connect(): connects the socket to another socket specified by IPAddress address and int port.
	\item getInputStream(): returns the rxp.RxPInputStream associated with the socket.
	\item getOutputStream(): returns the rxp.RxPOutputStream associated with the socket.
\end{enumerate}
\subsection{rxp.RxPInputStream}
\begin{enumerate}
	\item read(byte[] b, int length): reads length bytes from the rxp.RxPInputStream and writes them to the b byte array.
\end{enumerate}
\subsection{rxp.RxPOutputStream}
\begin{enumerate}
	\item write(byte[] b, int length): writes length bytes to the rxp.RxPOutputStream from the b byte array.
\end{enumerate}



\end{document}